# Beispiel senden und empfangen von Daten im TTN

Dieser Sketch ist das Beispiel welches bei der MKR WAN Lib mitgeliefert wird.
Es zeigt wie man Daten zum TTN sendet und aus dem TTN empfangen kann.
Die einzige Änderung ist diese Readme Datei

## Getting Started

Wir gehen davon aus das du die aktuelle GIT Version auf deinem Rechner installiert hast.
Eine Anleitung zur installation von GIT findest du hier: https://www.linode.com/docs/development/version-control/how-to-install-git-on-linux-mac-and-windows/
 
Um das Projekt herunter zu laden erstelle ein Verzeichnis auf deinem Rechner und führe den foldenden Befehl aus: git clone https://gitlab.com/iotssl/arduino-1300-mkr-wan.git
Damit werden die alle für den Arduino 1300 MKR WAN Sketche in das gewählte Verzeichnis importiert.

### Vorausetzungen

Es muss die aktuelle Arduino IDE von https://www.arduino.cc/en/Main/Software auf deinem Rechner installiert werden.

### Anleitung

* Trage in die Datei arduino_secrets.h deine eigenen TTN Keys für deinen Sensor ein.
* Baue und übertrage den Sketch an den Arduino 1300 MKR WAN
* öffne den seriellen Monitor in der Arduino IDE (Strg+Shift+M)
* Gebe deinen Test ein der gesendet werden soll

Als Rückgabe erscheint hoffentlich die Info das es funktioniert hat ;-)
Und das keine Daten vom TTN zum Node zu Übertragen waren.

Empfangen von Daten:

* gehe auf die TTN Console mit deinem Browser https://console.thethingsnetwork.org/
* gehe in deine Applikation mit der dein Node verbunden ist
* Wähle dein Device aus
* Dort findest du im unteren drittel der Seite eine Möglichkeit einen Downlink zu erstellen
* Das Feld Payload kann deinen Downlink Nachricht als Bytes in Hex Werten aufnehmen und mit Send wird eine Übertragung vorbereitet
* Die Übertragung wird aber nun erst einmal bereitgestellt, da das Downlink Fenster bei LoRaWAN erst nach einer Sendung geöffnet wird.
* Sende eine Nachricht aus dem Seriellen Monitor an das TTN wie im letzten Abschnitt beschrieben und deine Vorbereitete Nachricht wird danach an deinen Node versendet.  

Über die LoRaWAN Kommunikation kannst du bei https://iot-usergroup.de/ eine Menge lernen. Dort findetst du auch unsere LoRaWAN Grundlagenschulung https://iot-usergroup.de/projekte/workshop-lorawan-grundlagen/


## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/iotssl/arduino-1300-mkr-wan/-/tags). 

## Authors

Der Ursprung dieses Sketch ist das Beispiel von Arduino
Diese Anleitung wurden von Frank Radzio erstellt

## License

This project is licensed under the GNU General Public License v3.0 License - see the [LICENSE](https://gitlab.com/iotssl/arduino-1300-mkr-wan/blob/master/LICENSE) file for details


