# Arduino 1300 MKR WAN

Hier werden sich verschiedene Sketche für den Arduino 1300 MKR WAN zusammen finden.
Der MKR ist ein einfach zu programmierendes Arduino LoRaWAN Board das auch in vielen Schulen zur Ausbildung genutzt wird.

## Getting Started

Wir gehen davon aus das du die aktuelle GIT Version auf deinem Rechner installiert hast.
Eine Anleitung zur installation von GIT findest du hier: https://www.linode.com/docs/development/version-control/how-to-install-git-on-linux-mac-and-windows/

Um das Projekt herunter zu laden erstelle ein Verzeichnis auf deinem Rechner und führe den foldenden Befehl aus: git clone https://gitlab.com/iotssl/arduino-1300-mkr-wan.git
Damit werden die alle für den Arduino 1300 MKR WAN Sketche in das gewählte Verzeichnis importiert.

### Vorausetzungen

Es muss die aktuelle Arduino IDE von https://www.arduino.cc/en/Main/Software auf deinem Rechner installiert werden.
Eine zusätzliche Lib muss installiert werden https://github.com/thesolarnomad/lora-serialization.
Als zip aus dem Repo laden und in die Arduino IDE als neue Lib aus zip File einbinden.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/iotssl/arduino-1300-mkr-wan/-/tags).

## Authors

Der Ursprung dieser Sketche sind das Beispiel von Arduino für den 1300 MKR WAN und die MKR ENV Erweiterung
Diese Anleitung wurden von Frank Radzio erstellt

## License

This project is licensed under the GNU General Public License v3.0 License - see the [LICENSE](https://gitlab.com/iotssl/arduino-1300-mkr-wan/blob/master/LICENSE) file for details

