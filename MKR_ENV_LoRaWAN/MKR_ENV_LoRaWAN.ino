/*
 * Arduino 1300 MKR WAN mit MKR ENV Shield
 * Datenübertragung an das TTN via LoRaWAN
 * 
 * Basis sind die Beispiele der MKR WAN Lib und der MKR ENV Lib
 * 
 * Lizenz GNU GENERAL PUBLIC LICENSE Version 3
 * https://gitlab.com/iotssl/arduino-1300-mkr-wan/blob/master/LICENSE
 * 
 * Repo unter https://gitlab.com/iotssl/arduino-1300-mkr-wan
 * 
 * Author: Frank Radzio 
 * https://gitlab.com/DasNordlicht
 * 
 * 
 * Verwendete Libs:
 * 
 * https://github.com/arduino-libraries/MKRWAN
 * https://github.com/arduino-libraries/Arduino_MKRENV
 * 
 * Auswertung
 * https://grafana.ffslfl.net/d/xxlSNHbWz/schulwetter?orgId=1&refresh=10s
 * 
 * Payload Decoder für das TTN
 * 
 * function Decoder(bytes, port) {
var bytesToInt = function(bytes) {
  var i = 0;
  for (var x = 0; x < bytes.length; x++) {
    i |= +(bytes[x] << (x * 8));
  }
  return i;
};

var unixtime = function(bytes) {
  if (bytes.length !== unixtime.BYTES) {
    throw new Error('Unix time must have exactly 4 bytes');
  }
  return bytesToInt(bytes);
};
unixtime.BYTES = 4;

var uint8 = function(bytes) {
  if (bytes.length !== uint8.BYTES) {
    throw new Error('int must have exactly 1 byte');
  }
  return bytesToInt(bytes);
};
uint8.BYTES = 1;

var uint16 = function(bytes) {
  if (bytes.length !== uint16.BYTES) {
    throw new Error('int must have exactly 2 bytes');
  }
  return bytesToInt(bytes);
};
uint16.BYTES = 2;

var latLng = function(bytes) {
  if (bytes.length !== latLng.BYTES) {
    throw new Error('Lat/Long must have exactly 8 bytes');
  }

  var lat = bytesToInt(bytes.slice(0, latLng.BYTES / 2));
  var lng = bytesToInt(bytes.slice(latLng.BYTES / 2, latLng.BYTES));

  return [lat / 1e6, lng / 1e6];
};
latLng.BYTES = 8;

var temperature = function(bytes) {
  if (bytes.length !== temperature.BYTES) {
    throw new Error('Temperature must have exactly 2 bytes');
  }
  var isNegative = bytes[0] & 0x80;
  var b = ('00000000' + Number(bytes[0]).toString(2)).slice(-8)
        + ('00000000' + Number(bytes[1]).toString(2)).slice(-8);
  if (isNegative) {
    var arr = b.split('').map(function(x) { return !Number(x); });
    for (var i = arr.length - 1; i > 0; i--) {
      arr[i] = !arr[i];
      if (arr[i]) {
        break;
      }
    }
    b = arr.map(Number).join('');
  }
  var t = parseInt(b, 2);
  if (isNegative) {
    t = -t;
  }
  return t / 1e2;
};
temperature.BYTES = 2;

var humidity = function(bytes) {
  if (bytes.length !== humidity.BYTES) {
    throw new Error('Humidity must have exactly 2 bytes');
  }

  var h = bytesToInt(bytes);
  return h / 1e2;
};
humidity.BYTES = 2;

var bitmap = function(byte) {
  if (byte.length !== bitmap.BYTES) {
    throw new Error('Bitmap must have exactly 1 byte');
  }
  var i = bytesToInt(byte);
  var bm = ('00000000' + Number(i).toString(2)).substr(-8).split('').map(Number).map(Boolean);
  return ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
    .reduce(function(obj, pos, index) {
      obj[pos] = bm[index];
      return obj;
    }, {});
};
bitmap.BYTES = 1;

var decode = function(bytes, mask, names) {

  var maskLength = mask.reduce(function(prev, cur) {
    return prev + cur.BYTES;
  }, 0);
  if (bytes.length < maskLength) {
    throw new Error('Mask length is ' + maskLength + ' whereas input is ' + bytes.length);
  }

  names = names || [];
  var offset = 0;
  return mask
    .map(function(decodeFn) {
      var current = bytes.slice(offset, offset += decodeFn.BYTES);
      return decodeFn(current);
    })
    .reduce(function(prev, cur, idx) {
      prev[names[idx] || idx] = cur;
      return prev;
    }, {});
};

if (typeof module === 'object' && typeof module.exports !== 'undefined') {
  module.exports = {
    unixtime: unixtime,
    uint8: uint8,
    uint16: uint16,
    temperature: temperature,
    humidity: humidity,
    latLng: latLng,
    bitmap: bitmap,
    decode: decode
  };
}
  return decode(bytes, [uint8, uint16, uint16,unixtime,uint16,humidity,temperature], ['uvIndex', 'uvb', 'uva', 'illuminance', 'pressure', 'humidity', 'temperature']);
}

 * 
 * Version 0.1.0 im November 2019
 * 
 * Bei Fehlern nutzt bitte das Gitlab und erstellt ein Issue
 * https://gitlab.com/iotssl/arduino-1300-mkr-wan/issues
 * Gebt dabei bitte den Namen des sketch mit an.
 */

#include <MKRWAN.h> //Lib für das Board
#include <Arduino_MKRENV.h> //Lib für die Sensor Erweiterung
#include <LoraMessage.h> // Lib für die Lora Serialisierung 
#include <LoraEncoder.h>

LoRaModem modem;

//byte buffer[15];
//LoraEncoder encoder(buffer);

// Uncomment if using the Murata chip as a module
// LoRaModem modem(Serial1);

#include "arduino_secrets.h"   // einbinden der geheimen schlüsel aus der arduino_secrets.h
String appEui = SECRET_APP_EUI;
String appKey = SECRET_APP_KEY;

void setup() {
  // Hier werden die Einstellungen eingetragen die einmal beim Start durchlaufen werden
  Serial.begin(115200);
//  while (!Serial);  //Warten auf den Seriellen Monitor
  // Bitte wähle das richtige Regionale Band aus (eg. US915, AS923, ...)
  if (!modem.begin(EU868)) {
    Serial.println("Das LoRa Modem konnte nicht gestartet werden");
    while (1) {}
  };
  
    if (!ENV.begin()) {
    Serial.println("Failed to initialize MKR ENV shield!");
    while (1);
  }
  
  Serial.print("Deine module version ist: ");
  Serial.println(modem.version());
  Serial.print("Dieses Gerät hat folgende Device EUI (Zur Einrichtung im Netz notwendig): ");
  Serial.println(modem.deviceEUI());

  int connected = modem.joinOTAA(appEui, appKey);
 
  // Der folgende Abschitt würde den Code anhalten bis eine Verbindung zum TTN möglich wird. Gut zum Testen aber unpraktisch im Feldeinsatz.
  // Bei Tests kann man sich diesen Abschnitt freischalten in dem man den Kommentar in den nächsten 4 Zeilen entfent.
//  if (!connected) {
//    Serial.println("Es hat irgend etwas nicht funktioniert; Bist du in einem Raum der keine Verbindung zu einem Gateway ermöglicht? Versuche es noch einmal in der Nähe eines Fensters");
//    while (1) {}
//  }

  // Set poll interval to 120 secs.
  modem.minPollInterval(120);
  // NOTE: unabhängig von dieser Einstellung erlaubt das Modem nicht 
  //       mehr als eine Nachricht alle 2 Minuten zu senden,
  //       Dies wird durch Firmware erzwungen und kann nicht geändert werden.
}

void loop() {
  // Dies ist die Hauptschleife, diese wird ständig abgearbeitet.
  byte buffer[15];
  LoraEncoder encoder(buffer);


  // alle Sensoren auslesen und in die Variablen sichern
  float temperature = ENV.readTemperature();  //Temperature range -40 +120; accuracy: ± 0.5 °C, 15 to +40 °C
  float humidity    = ENV.readHumidity();  //Humidity range: 0 - 100%; accuracy: ± 3.5% rH, 20 to +80% rH 
  float pressure    = ENV.readPressure(MILLIBAR);  //pressure range: 260 to 1260 hPa 
  float illuminance = ENV.readIlluminance();  //Lux range of the sensor with analog reading from 10 to 100.000 lux
  float uva         = ENV.readUVA(); //UVA/UVB resolution: 16bit; unit μW/cm2 
  float uvb         = ENV.readUVB(); //UVA/UVB resolution: 16bit; unit μW/cm2 
  float uvIndex     = ENV.readUVIndex(); //UVIndex: from 1 to 11+  

  // print each of the sensor values
  Serial.print("Temperature = ");
  Serial.print(temperature);
  Serial.println(" °C");

  Serial.print("Humidity    = ");
  Serial.print(humidity);
  Serial.println(" %");

  Serial.print("Pressure    = ");
  Serial.print(pressure);
  Serial.println(" milibar");

  Serial.print("Illuminance = ");
  Serial.print(illuminance);
  Serial.println(" lx");

  Serial.print("UVA         = ");
  Serial.println(uva);

  Serial.print("UVB         = ");
  Serial.println(uvb);

  Serial.print("UV Index    = ");
  Serial.println(uvIndex);

  // print an empty line
  Serial.println();

  // befülle den Buffer der an das TTN übertragen wird
    
  encoder.writeUint8(uvIndex);  // 1 Byte
  encoder.writeUint16(uvb);     // 2 Byte
  encoder.writeUint16(uva);     // 2 Byte
  encoder.writeUnixtime(illuminance); // 4 Byte
  encoder.writeUint16(pressure); // 2 Byte
  encoder.writeHumidity(humidity);// 2 Byte
  encoder.writeTemperature(temperature);  // 2 Byte
  
  // Ausgabe des Buffers im Monitor
  for (unsigned int i = 0; i < 15; i++) {
    Serial.print(buffer[i] >> 4, HEX);
    Serial.print(buffer[i] & 0xF, HEX);
    Serial.print(" ");
  }
  Serial.println();

 // Senden und Empfangen der Daten
  int err;
  modem.beginPacket();
  modem.write(buffer,15);
  err = modem.endPacket(true);
  /* This is the block that throws an exception on the MKR ENV 1310:
  if (err > 0) {
    Serial.println("Message sent correctly!");
  } else {
    Serial.println("Error sending message :(");
    Serial.println("(you may send a limited amount of messages per minute, depending on the signal strength");
    Serial.println("it may vary from 1 message every couple of seconds to 1 message every minute)");
  }
  delay(1000);
  if (!modem.available()) {
    Serial.println("No downlink message received at this time.");
    return;
  }
  char rcv[64];
  int i = 0;
  while (modem.available()) {
    rcv[i++] = (char)modem.read();
  }
  Serial.print("Received: ");
  for (unsigned int j = 0; j < i; j++) {
    Serial.print(rcv[j] >> 4, HEX);
    Serial.print(rcv[j] & 0xF, HEX);
    Serial.print(" ");
  }
  Serial.println();
  */
  
  // dirty hack: the MKR WAN 1310 seems to send messages ignoring the minPollInterval
  //   this results in a fair use rule conflict and the device gets blocked
  delay(30000);
}
